using System;
using System.Windows;
using RiftDotNet;

namespace VSMVrDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly IHMD _hmd;
        private readonly HMDManager _hmdManager;

        public MainWindow()
        {
            InitializeComponent();

            _hmdManager = new HMDManager();
            _hmd = _hmdManager.AttachedDevice;
            if (_hmd == null)
            {
                MessageBox.Show("Please attach your VR headset.");
                _hmd = _hmdManager.WaitForAttachedDevice(null);
            }

            DeviceInfo.DataContext = _hmd.Info;
            _hmd.Reset();
            OrientationInfo.DataContext = _hmd;

            var dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += UpdateLoop;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 16);
            dispatcherTimer.Start();
        }

        private void UpdateLoop(object sender, EventArgs eventArgs)
        {
            OrientationInfo.DataContext = null;
            OrientationInfo.DataContext = _hmd;
        }
    }
}