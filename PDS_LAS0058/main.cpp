/*
 * author: Adam Lasak <lasak.ad@gmail.com>
 * subject: Parallel and Distributing systems
 * academic year: 2018
 */

#include <algorithm>
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <omp.h>

using namespace std;

const string ERR_ARGUMENT_MISSING = "missing argument ";
const string ERR_ARGUMENT_1 = "1: method";
const string ERR_ARGUMENT_2 = "2: filename";
const string ERR_ARGUMENT_3 = "3: count of threads";
const string ERR_ARGUMENT_4 = "4: expression";
const string ERR_ARGUMENT_HELP = "?: see README.md for documentation";
const string ERR_UNABLE_OPEN_FILE = "unable to open file: ";
const string ERR_EXPRESSION_NOT_FOUND = "expression not found: ";


int main(int argc, char** argv) {
    
    string path, expression;
    int countOfThreads, countOfLines = 0, method;
    vector<string> *lines = new vector<string>();
    clock_t start, finish;

    if (argc == 5){
        
        method = atoi(argv[1]);
        path = argv[2];
        countOfThreads = atoi(argv[3]);
        expression = argv[4];
        
        if (method < 0 || method > 1){
            cout << ERR_ARGUMENT_MISSING << ERR_ARGUMENT_1 << endl;
            return 0;
        }
        
        if (path.empty()){
            cout << ERR_ARGUMENT_MISSING << ERR_ARGUMENT_2 << endl;
            return 0;
        }
        
        if (countOfThreads <= 0){
            cout << ERR_ARGUMENT_MISSING << ERR_ARGUMENT_3 << endl;
            return 0;
        }
        
        if (expression.empty()){
            cout << ERR_ARGUMENT_MISSING << ERR_ARGUMENT_4 << endl;
            return 0;
        }
       
        omp_set_num_threads(countOfThreads);
        

        ifstream file(path, ifstream::binary);
        
        if (file){
            
            transform(expression.begin(), expression.end(), expression.begin(), ::tolower);
            
            string line; int lineNum = 0;
            int linesPerThread = 0, linesForLastThread = 0;
            bool found = false;
            
            if (method == 0){
                
                start = clock();

                while(getline(file, line)){
                    ++lineNum;
                    if (line.find(expression) != string::npos) {
                            found = true;
                            cout << lineNum << ": " << line << endl;
                    }
                }
                
                finish = clock();
                cout << "Time (seconds): " << ((double)(finish - start))/CLOCKS_PER_SEC << endl;
                
            } else {
                
                while (getline(file, line)) {
                    lines->push_back(line);
                }
                countOfLines = lines->size();

                linesPerThread = (int)(countOfLines / countOfThreads);

                if ((linesPerThread * (countOfThreads - 1)) < countOfLines){
                    linesForLastThread = countOfLines - (linesPerThread * (countOfThreads - 1));
                }
                
                start = clock();
                int pid, linesForThread, endOfCycle;
                #pragma omp parallel private(pid) private(linesForThread) private(endOfCycle) reduction(+:found)
                {
                    pid = omp_get_thread_num();
                    if (pid == countOfThreads - 1){
                        linesForThread = linesForLastThread;
                        endOfCycle = countOfLines;
                    } else {
                        linesForThread = linesPerThread;
                        endOfCycle = (pid + 1) * linesForThread;
                    }

                    #pragma omp parallel for schedule(static, 2048)
                    for (int i = (pid * linesForThread); i < endOfCycle; ++i){
                        if (lines->at(i).find(expression) != string::npos) {
                            found = true;
                            cout << i+1 << ": " << lines->at(i) << endl;
                        }
                    }

                }
                
                finish = clock();
                cout << "Time (seconds): " << ((double)(finish - start))/CLOCKS_PER_SEC << endl;
            
            }
            
            if (!found){
                cout << ERR_EXPRESSION_NOT_FOUND << expression << endl;
            }
                
            file.close();
    
        } else {
            cout << ERR_UNABLE_OPEN_FILE << path << endl;
        }
        
    } else {
        cout << ERR_ARGUMENT_MISSING << ERR_ARGUMENT_HELP << endl;
    }
    
    return 0;
    
}
