#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=OpenMPI-Linux
CND_ARTIFACT_DIR_Debug=dist/Debug/OpenMPI-Linux
CND_ARTIFACT_NAME_Debug=pds_las0058
CND_ARTIFACT_PATH_Debug=dist/Debug/OpenMPI-Linux/pds_las0058
CND_PACKAGE_DIR_Debug=dist/Debug/OpenMPI-Linux/package
CND_PACKAGE_NAME_Debug=pdslas0058.tar
CND_PACKAGE_PATH_Debug=dist/Debug/OpenMPI-Linux/package/pdslas0058.tar
# Release configuration
CND_PLATFORM_Release=OpenMPI-Linux
CND_ARTIFACT_DIR_Release=dist/Release/OpenMPI-Linux
CND_ARTIFACT_NAME_Release=pds_las0058
CND_ARTIFACT_PATH_Release=dist/Release/OpenMPI-Linux/pds_las0058
CND_PACKAGE_DIR_Release=dist/Release/OpenMPI-Linux/package
CND_PACKAGE_NAME_Release=pdslas0058.tar
CND_PACKAGE_PATH_Release=dist/Release/OpenMPI-Linux/package/pdslas0058.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
